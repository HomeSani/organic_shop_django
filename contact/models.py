from django.db import models


class Info(models.Model):
    """Info(about company) model, must be one"""

    phone = models.CharField(max_length=16, verbose_name='Телефон')
    address = models.CharField(max_length=64, verbose_name='Адрес')
    start_work_time = models.TimeField(verbose_name='Время начало работы(вида 24:60:60)')
    end_work_time = models.TimeField(verbose_name='Время окончания работы(вида 24:60:60)')
    email = models.CharField(max_length=32, verbose_name='Почта')

    class Meta:
        verbose_name = "Информация"
        verbose_name_plural = "Информация"

    def __str__(self):
        return f'{self.phone}, {self.address}'


class SocialMedia(models.Model):
    """Model for social media"""

    icon = models.ImageField(upload_to='social_icons/', verbose_name='Иконка')
    link = models.URLField(max_length=200, verbose_name='Ссылка')

    class Meta:
        verbose_name = "Социальная сеть"
        verbose_name_plural = "Социальные сети"

    def __str__(self):
        return self.link


class Message(models.Model):
    """Users message for administration"""

    name = models.CharField(max_length=64, verbose_name='Имя пользователя')
    email = models.EmailField(max_length=64, verbose_name='Почта пользователя')
    text = models.CharField(max_length=256, verbose_name='Сообщение')

    class Meta:
        verbose_name = "Письмо"
        verbose_name_plural = "Письма"

    def __str__(self):
        return self.name
