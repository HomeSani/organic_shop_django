from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import View

from accounts.models import Account
from contact.forms import MessageForm
from contact.models import Info, Message


class ContactView(View):
    def get(self, request):
        info = Info.objects.first()
        account = None

        if request.user.id:
            account = Account.objects.get(user_id=request.user.id)

        return render(request, 'contact.html',
                      {
                          'info': info,
                          'account': account,
                      })

    def post(self, request):
        form = MessageForm(request.POST)

        if form.is_valid():
            name = form.cleaned_data.get('name')
            email = form.cleaned_data.get('email')
            text = form.cleaned_data.get('text')

            Message.objects.create(name=name, email=email, text=text)

        return redirect(reverse('contact'))
