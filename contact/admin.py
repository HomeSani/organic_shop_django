from django.contrib import admin

from contact.models import Info, SocialMedia, Message

admin.site.register([Info, SocialMedia])


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ['name', 'email']
