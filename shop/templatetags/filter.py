from django import template

register = template.Library()


@register.filter(name='sale_value')
def get_sale_value(value):
    """Return sale value as format: -20%"""

    sale_value = round(((value.price - value.sale_price) * 100) / value.price)

    return f'-{sale_value}%'
