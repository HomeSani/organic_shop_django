from typing import Any

from django.db.models import QuerySet

from shop.forms import OrderForm
from shop.models import FavoriteProduct, Product, Cart


def get_rated_and_popular_products(products: QuerySet) -> tuple[list[Any], list[Any]]:
    """Return popular(more reviewed) and rated products"""

    products_rates = {}
    products_reviews_count = {}

    for product in products:
        review_rates = [review.stars_count for review in product.reviews.all()]
        rating = 0

        if len(review_rates) != 0:
            rating = sum(review_rates) / len(review_rates)

        products_rates[product] = rating
        products_reviews_count[product] = len(review_rates)

    products_rates = sorted(products_rates.items(), key=lambda x: x[1], reverse=True)[:3]
    products_reviews_count = sorted(products_reviews_count.items(), key=lambda x: x[1], reverse=True)[:3]

    rated_products = list(map(lambda item: item[0], products_rates))
    popular_products = list(map(lambda item: item[0], products_reviews_count))

    return rated_products, popular_products


def is_favorite_product(product: Product, user_id: int) -> bool:
    """Check product is favorite"""

    try:
        if FavoriteProduct.objects.get(user_id=user_id, product=product):
            return True
    except FavoriteProduct.DoesNotExist:
        return False


def get_stars_count(reviews: QuerySet) -> int:
    """Return stars count by reviews of product"""

    if reviews:
        return round(sum([review.stars_count for review in reviews]) / len(reviews))

    return 0


def get_sum_of_cart(cart: Cart) -> int:
    """Return sum of cart by cart items"""

    total_sum = 0

    for cart_item in cart.cart_items.all():
        if cart_item.product.is_sale:
            total_sum += cart_item.product.sale_price * cart_item.quantity
        else:
            total_sum += cart_item.product.price * cart_item.quantity

    return total_sum


def create_order_and_clean_cart(order_form: OrderForm, cart: Cart, user_id: int) -> None:
    """Create Order object and make cart empty"""

    order = order_form.save(commit=False)
    order.price = get_sum_of_cart(cart)
    order.user_id = user_id
    order.save()

    for cart_item in cart.cart_items.all():
        order.products.add(cart_item.product)
        cart_item.delete()
