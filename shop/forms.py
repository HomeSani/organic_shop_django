from django import forms

from shop.models import Order


class ShopFilterForm(forms.Form):
    category = forms.CharField(max_length=32)
    color = forms.CharField(max_length=32)
    size = forms.CharField(max_length=32)
    min_price = forms.CharField(max_length=16)
    max_price = forms.CharField(max_length=16)


class AddToCartForm(forms.Form):
    quantity = forms.IntegerField()


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        exclude = ['created_at', 'price', 'user', 'is_complete', 'products']


class ReviewForm(forms.Form):
    text = forms.CharField(max_length=256)
    stars_count = forms.IntegerField(min_value=1, max_value=5)


class AddToFavoriteForm(forms.Form):
    is_favorite = forms.BooleanField(required=False)
