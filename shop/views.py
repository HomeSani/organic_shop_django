from urllib.parse import urlencode

from django.core.paginator import Paginator
from django.db.models import Q
from django.shortcuts import render, redirect, reverse
from django.views import View

from accounts.models import Account
from blog.models import Post
from shop import utils
from shop.const import PRODUCT_COLORS, PRODUCT_SIZES
from shop.forms import ShopFilterForm, AddToCartForm, OrderForm, ReviewForm, AddToFavoriteForm
from shop.models import Product, Category, Cart, CartItem, Review, FavoriteProduct


class IndexView(View):
    def get(self, request):
        categories = Category.objects.all()
        products = Product.objects.all()
        last_added_products = products.order_by('-created_at')[:3]
        rated_products, popular_products = utils.get_rated_and_popular_products(products)
        posts = Post.objects.order_by('-created_at')[:3]

        context = {
            'categories': categories,
            'rated_products': rated_products,
            'last_added_products': last_added_products,
            'popular_products': popular_products,
            'posts': posts,
        }

        return render(request, 'index.html', context)


class ProductDetailView(View):
    def get(self, request, slug):
        product = Product.objects.get(slug=slug)
        related_products = Product.objects.exclude(id=product.id).filter(
            Q(category=product.category) | Q(color=product.color))[:4]

        reviews = product.reviews.all()
        reviews_count = len(reviews)
        stars_count = utils.get_stars_count(reviews)

        is_favorite = utils.is_favorite_product(product=product, user_id=request.user.id)

        context = {
            'product': product,
            'related_products': related_products,
            'reviews': reviews,
            'reviews_count': reviews_count,
            'stars_count': stars_count,
            'is_favorite': is_favorite,
        }

        return render(request, 'product_detail.html', context)

    def post(self, request, slug):
        product = Product.objects.get(slug=slug)

        if request.POST.get('text', None):
            form = ReviewForm(request.POST)

            if form.is_valid():
                text = form.cleaned_data.get('text')
                stars_count = form.cleaned_data.get('stars_count')
                account = Account.objects.get(user_id=request.user.id)

                Review.objects.create(author=account, product=product, text=text, stars_count=stars_count)
        elif request.POST.get('quantity', None):
            cart = Cart.objects.get(user_id=request.user.id)
            form = AddToCartForm(request.POST)

            if form.is_valid():
                quantity = form.cleaned_data.get('quantity')

                CartItem.objects.create(cart=cart, product=product, quantity=quantity)

                return redirect(reverse('shop'))
        else:
            form = AddToFavoriteForm(request.POST)

            if form.is_valid():
                is_favorite = form.cleaned_data.get('is_favorite')
                product = Product.objects.get(slug=slug)

                if is_favorite:
                    FavoriteProduct.objects.create(user_id=request.user.id, product=product)
                else:
                    FavoriteProduct.objects.get(user_id=request.user.id, product=product).delete()

        return redirect(reverse('product_detail', kwargs={'slug': slug}))


class ShopView(View):
    def get(self, request):
        products = Product.objects.all()

        category = request.GET.get('category') or None
        color = request.GET.get('color') or None
        size = request.GET.get('size') or None
        min_price = request.GET.get('min_price') or None
        max_price = request.GET.get('max_price') or None

        if category:
            products = products.filter(category__name=category)
        if color:
            products = products.filter(color=color)
        if size:
            products = products.filter(size=size)
        if min_price and max_price:
            products = products.filter(price__range=(int(min_price), int(max_price)))

        products_with_sale = Product.objects.filter(is_sale=True)
        last_added_products = Product.objects.order_by('-created_at')[:3]

        categories = Category.objects.all()

        paginator = Paginator(products, 12)

        page_number = request.GET.get('page')
        page = paginator.get_page(page_number)

        context = {
            'find_products_count': len(products),
            'products_with_sale': products_with_sale,
            'page': page,
            'categories': categories,
            'colors': PRODUCT_COLORS,
            'sizes': PRODUCT_SIZES,
            'last_added_products': last_added_products,
            'filters': {
                'category': category,
                'color': color,
                'size': size,
                'price': {
                    'min': min_price,
                    'max': max_price
                }
            }
        }

        return render(request, 'shop.html', context)

    def post(self, request):
        form = ShopFilterForm(request.POST)

        category = form['category'].value() or ''
        color = form['color'].value() or ''
        size = form['size'].value() or ''
        min_price = form['min_price'].value().split()[-1] or ''
        max_price = form['max_price'].value().split()[-1] or ''

        params = {
            'category': category,
            'color': color,
            'size': size,
            'min_price': min_price,
            'max_price': max_price
        }

        return redirect(f"{reverse('shop')}?{urlencode(params)}")


class CartView(View):
    def get(self, request):
        cart = Cart.objects.get(user=request.user)

        cart_items = cart.cart_items.all()
        total_sum = utils.get_sum_of_cart(cart)

        context = {
            'cart_items': cart_items,
            'total_sum': total_sum,
        }

        return render(request, 'cart.html', context)

    def post(self, request):
        cart_item_id = int(request.POST.get('product_id'))
        cart_item = CartItem.objects.get(id=cart_item_id)

        cart_item.delete()

        return redirect(reverse('cart'))


class CheckoutView(View):
    def get(self, request):
        cart = Cart.objects.get(user=request.user)

        cart_items = cart.cart_items.all()
        total_sum = utils.get_sum_of_cart(cart)

        context = {
            'cart_items': cart_items,
            'total_sum': total_sum,
        }

        return render(request, 'checkout.html', context)

    def post(self, request):
        cart = Cart.objects.get(user=request.user)
        form = OrderForm(request.POST)

        if form.is_valid():
            utils.create_order_and_clean_cart(form, cart, request.user.id)

        return redirect(reverse('checkout'))


class FavoriteProductsView(View):
    def get(self, request):
        products = FavoriteProduct.objects.filter(user=request.user)

        return render(request, 'favorite_products.html', {'products': products})
