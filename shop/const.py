PRODUCT_SIZES = [
    ('tiny', 'Крошечный'),
    ('small', 'Маленький'),
    ('medium', 'Средний'),
    ('large', 'Большой'),
]

PRODUCT_COLORS = [
    ('red', 'Красный'),
    ('yellow', 'Жёлтый'),
    ('blue', 'Голубой'),
    ('black', 'Чёрный'),
    ('pink', 'Розовый'),
    ('purple', 'Фиолетовый'),
    ('green', 'Зелёный'),
]

PAYMENT_TYPES = [
    ('cash', 'Наличными'),
    ('transfer', 'Переводом'),
]
