from django.contrib.auth.models import User
from django.db import models

from accounts.models import Account
from shop.const import PRODUCT_SIZES, PRODUCT_COLORS, PAYMENT_TYPES


class Category(models.Model):
    """Model for categories"""

    name = models.CharField(max_length=128, verbose_name='Имя')
    slug = models.SlugField(max_length=128, blank=True, verbose_name='Слаг')

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"

    def __str__(self):
        return self.name


class Product(models.Model):
    """Model for products"""

    name = models.CharField(max_length=128, verbose_name='Имя')
    slug = models.SlugField(max_length=128, blank=True, verbose_name='Слаг')
    description = models.CharField(max_length=512, verbose_name='Описание')
    category = models.ForeignKey(
        Category, on_delete=models.PROTECT, related_name="products", verbose_name='Категория'
    )
    price = models.PositiveIntegerField(verbose_name="Цена продукта в рублях")
    weight = models.FloatField(verbose_name="Вес продукта в кг")
    image = models.ImageField(upload_to="product_images/")
    size = models.CharField(max_length=32, choices=PRODUCT_SIZES, verbose_name='Размер')
    color = models.CharField(max_length=32, choices=PRODUCT_COLORS, verbose_name='Цвет')
    availability = models.BooleanField(default=True, verbose_name='Доступен ли товар')
    is_sale = models.BooleanField(default=False, verbose_name="Распродаётся ли товар")
    sale_price = models.PositiveIntegerField(
        verbose_name="Цена для распродажи", blank=True, null=True
    )
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "Продукт"
        verbose_name_plural = "Продукты"

    def __str__(self):
        return f"{self.name} - {self.price}"


class Review(models.Model):
    """Model for product reviews"""

    author = models.ForeignKey(Account, on_delete=models.CASCADE, related_name="reviews", verbose_name='Автор')
    product = models.ForeignKey(
        Product, on_delete=models.PROTECT, related_name="reviews", verbose_name='Продукт'
    )
    text = models.CharField(max_length=256, verbose_name='Текст')
    stars_count = models.PositiveIntegerField(verbose_name='Кол-во звёзд(от 1 до 5)')

    class Meta:
        verbose_name = "Отзыв"
        verbose_name_plural = "Отзывы"

    def __str__(self):
        return f"{self.author} review {self.product}"


class Cart(models.Model):
    """
    Model for shop cart,
    use CartItem for products as thought models
    """

    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name='Пользователь')
    products = models.ManyToManyField(Product, through="CartItem", verbose_name='Продукты')

    class Meta:
        verbose_name = "Корзина"
        verbose_name_plural = "Корзины"

    def __str__(self):
        return f"{self.user}: {self.products}"


class CartItem(models.Model):
    """Model for cart item"""

    cart = models.ForeignKey(Cart, on_delete=models.CASCADE, related_name="cart_items", verbose_name='Корзина')
    product = models.ForeignKey(
        Product, on_delete=models.CASCADE, related_name="in_cart", verbose_name='Продукт'
    )
    quantity = models.PositiveIntegerField(default=1, verbose_name='Кол-во')

    class Meta:
        verbose_name = "Товар в корзине"
        verbose_name_plural = "Товары в корзине"

    def __str__(self):
        return f"{self.cart}: {self.product}"


class Order(models.Model):
    """Model for order"""

    user = models.ForeignKey(User, on_delete=models.PROTECT, null=True, verbose_name='Пользователь')
    first_name = models.CharField(max_length=128, verbose_name='Имя')
    last_name = models.CharField(max_length=128, verbose_name='Фамилия')
    street = models.CharField(max_length=128, verbose_name='Улица')
    house = models.CharField(max_length=128, verbose_name='Дом')
    city = models.CharField(max_length=128, verbose_name='Город')
    post_index = models.PositiveIntegerField(verbose_name='Почтовый индекс')
    phone_number = models.CharField(max_length=128, verbose_name='Номер телефона')
    comment = models.CharField(max_length=256, verbose_name='Комментарий кдоставке')
    payment_type = models.CharField(max_length=128, choices=PAYMENT_TYPES, verbose_name='Способ оплаты')
    price = models.PositiveIntegerField(default=1, verbose_name='Цена заказа')
    products = models.ManyToManyField(Product, related_name="orders", verbose_name='Продукты')
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    is_complete = models.BooleanField(default=False, verbose_name='Выполнен')

    class Meta:
        verbose_name = "Заказ"
        verbose_name_plural = "Заказы"

    def __str__(self):
        return f"{self.first_name} {self.last_name} - {self.phone_number}"


class FavoriteProduct(models.Model):
    """Model for favorite products"""

    user = models.ForeignKey(User, related_name='favorite_products', on_delete=models.PROTECT,
                             verbose_name='Пользователь')
    product = models.ForeignKey(
        Product, on_delete=models.PROTECT, related_name="favorites", verbose_name='Продукт'
    )

    class Meta:
        verbose_name = "Любимый продукт"
        verbose_name_plural = "Любимые продукты"

    def __str__(self):
        return f"{self.user}: {self.product}"
