from django.urls import path

from shop.views import IndexView, ProductDetailView, ShopView, CartView, CheckoutView, FavoriteProductsView

urlpatterns = [
    path('', IndexView.as_view(), name='shop_index'),
    path('product/<slug:slug>/', ProductDetailView.as_view(), name='product_detail'),
    path('shop/', ShopView.as_view(), name='shop'),
    path('cart/', CartView.as_view(), name='cart'),
    path('checkout/', CheckoutView.as_view(), name='checkout'),
    path('favorites/', FavoriteProductsView.as_view(), name='favorite_products'),
]
