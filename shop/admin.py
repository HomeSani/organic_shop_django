from django.contrib import admin

from shop.models import Product, Category, Review, Order


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'last_name', 'city', 'street', 'house', 'price', 'is_complete']
    list_editable = ['is_complete']


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'category', 'price', 'is_sale', 'sale_price', 'size', 'color', 'weight']
    list_editable = ['is_sale', 'sale_price', 'price']
    prepopulated_fields = {'slug': ('name',)}


@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
    list_display = ['author', 'product', 'stars_count']
