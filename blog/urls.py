from django.urls import path

from blog.views import PostDetailView, IndexView

urlpatterns = [
    path('', IndexView.as_view(), name='blog_index'),
    path('post/<slug:slug>', PostDetailView.as_view(), name='post_detail'),
]
