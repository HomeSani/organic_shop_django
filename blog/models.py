from django.db import models

from accounts.models import Account


class Tag(models.Model):
    """Tag model"""

    name = models.CharField(max_length=32, verbose_name='Имя')
    slug = models.SlugField(max_length=64, verbose_name='Слаг')

    class Meta:
        verbose_name = "Тег"
        verbose_name_plural = "Теги"

    def __str__(self):
        return self.name


class Category(models.Model):
    """Category model"""

    name = models.CharField(max_length=32, verbose_name='Имя')
    slug = models.SlugField(max_length=64, verbose_name='Слаг')

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"

    def __str__(self):
        return self.name


class Post(models.Model):
    """Post model"""

    author = models.ForeignKey(Account, related_name='posts', on_delete=models.CASCADE, verbose_name='Автор')
    title = models.CharField(max_length=128, verbose_name='Заголовок')
    sub_title = models.CharField(max_length=128, verbose_name='Под заголовок')
    slug = models.SlugField(max_length=256, verbose_name='Слаг')
    text = models.CharField(max_length=2048, verbose_name='Текст')
    tags = models.ManyToManyField(Tag, related_name='posts', verbose_name='Теги')
    category = models.ForeignKey(Category, related_name='posts', on_delete=models.CASCADE, verbose_name='Категория')
    created_at = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to='posts', verbose_name='Обложка')

    class Meta:
        verbose_name = "Пост"
        verbose_name_plural = "Посты"

    def __str__(self):
        return f'{self.title}({self.author})'
