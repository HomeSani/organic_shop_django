from django.contrib import admin

from blog.models import Post, Tag, Category

admin.site.register([Category])


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ['author', 'title', 'sub_title', 'category']
    prepopulated_fields = {'slug': ('title',)}
