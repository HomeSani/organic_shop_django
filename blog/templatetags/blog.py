from django.template import Library

from blog.models import Category, Post

register = Library()


@register.inclusion_tag('blog_sidebar.html', takes_context=True)
def sidebar(context):
    categories = Category.objects.all()
    recently_added_posts = Post.objects.order_by('-created_at')[:3]

    return {
        'categories': categories,
        'recently_added_posts': recently_added_posts,
    }
