from django.core.paginator import Paginator
from django.shortcuts import render
from django.views import View

from blog.models import Post


class PostDetailView(View):
    def get(self, request, slug):
        post = Post.objects.get(slug=slug)
        related_posts = Post.objects.filter(category=post.category).exclude(slug=slug)[:3]

        return render(request, 'blog_detail.html', {'post': post, 'related_posts': related_posts})


class IndexView(View):
    def get(self, request):
        category_slug = request.GET.get('category_slug')
        posts = []

        if (not category_slug) or category_slug == 'all':
            posts = Post.objects.all()
        else:
            posts = Post.objects.filter(category__slug=category_slug)

        paginator = Paginator(posts, 6)

        page_number = request.GET.get('page')
        page = paginator.get_page(page_number)

        return render(request, 'blog_index.html', {'page': page})
