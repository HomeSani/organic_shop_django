from rest_framework.routers import DefaultRouter

from api import views

router = DefaultRouter()
router.register(r'products', views.ProductViewSet)
router.register(r'posts', views.PostViewSet)

urlpatterns = router.urls
