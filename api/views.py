from rest_framework import viewsets, permissions
from rest_framework.decorators import action
from rest_framework.response import Response

from api import serializers
from blog.models import Post, Category as BlogCategory, Tag
from shop.const import PRODUCT_COLORS, PRODUCT_SIZES
from shop.models import Product, Category as ShopCategory, Review


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = serializers.ProductSerializer
    permission_classes = [permissions.AllowAny]

    def get_queryset(self):
        category = self.request.query_params.get('category')
        color = self.request.query_params.get('color')
        price = self.request.query_params.get('price_range')
        is_sale = self.request.query_params.get('is_sale')

        queryset = Product.objects.all()

        if category:
            queryset = queryset.filter(category_id=category)
        if color:
            queryset = queryset.filter(color=color)
        if price:
            price = price.split(',')
            min_price = int(price[0])
            max_price = int(price[1])

            queryset = queryset.filter(price__range=[min_price, max_price])
        if is_sale:
            if is_sale == 'true':
                is_sale = True
            else:
                is_sale = False

            queryset = queryset.filter(is_sale=is_sale)

        return queryset

    def const_to_dict(self, const):
        result = {}

        for pair in const:
            result[pair[0]] = pair[1]

        return result

    @action(methods=['get'], detail=False)
    def categories(self, request):
        categories = ShopCategory.objects.all()

        return Response(serializers.ProductCategorySerializer(categories, many=True).data)

    @action(methods=['get', 'post'], detail=True)
    def reviews(self, request, pk):
        product = Product.objects.get(pk=pk)

        if self.request.method == 'GET':
            reviews = Review.objects.filter(product=product)

            return Response(serializers.ProductReviewSerializer(reviews, many=True).data)

        if self.request.method == 'POST':
            serializer = serializers.ProductReviewSerializer(data=self.request.data)

            if serializer.is_valid():
                serializer.save()

                return Response({'message': 'New review is added.'})
            else:
                return Response(serializer.errors)

    @action(methods=['get'], detail=False)
    def colors(self, request):
        return Response(self.const_to_dict(PRODUCT_COLORS))

    @action(methods=['get'], detail=False)
    def sizes(self, request):
        return Response(self.const_to_dict(PRODUCT_SIZES))


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = serializers.PostSerializer
    permission_classes = [permissions.AllowAny]

    def get_queryset(self):
        category = self.request.query_params.get('category')
        tags = self.request.query_params.get('tags')

        queryset = Post.objects.all()

        if category:
            queryset = queryset.filter(category_id=category)
        if tags:
            tags = tags.split(',')
            tags = map(lambda tag: int(tag), tags)

            for tag in tags:
                queryset = queryset.filter(tags=tag)

        return queryset

    @action(methods=['get'], detail=False)
    def categories(self, request):
        categories = BlogCategory.objects.all()

        return Response(serializers.BlogCategorySerializer(categories, many=True).data)

    @action(methods=['get'], detail=False)
    def tags(self, request):
        tags = Tag.objects.all()

        return Response(serializers.BlogTagSerializer(tags, many=True).data)
