from rest_framework import serializers

from accounts.models import Account
from blog.models import Post, Tag, Category as BlogCategory
from shop.models import Product, Category as ShopCategory, Review


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'


class ProductCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ShopCategory
        exclude = ['slug']


class ProductReviewSerializer(serializers.Serializer):
    author_id = serializers.IntegerField()
    text = serializers.CharField(max_length=256)
    product_id = serializers.IntegerField()
    stars_count = serializers.IntegerField()

    def create(self, validated_data):
        review = Review.objects.create(
            author=Account.objects.get(pk=validated_data['author_id']),
            product=Product.objects.get(pk=validated_data['product_id']),
            text=validated_data['text'],
            stars_count=validated_data['stars_count']
        )

        return review

    def update(self, instance, validated_data):
        instance.text = validated_data['text']
        instance.stars_count = validated_data['stars_count']


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = '__all__'


class BlogCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = BlogCategory
        exclude = ['slug']


class BlogTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        exclude = ['slug']
