from django import template

from contact.models import SocialMedia, Info
from shop.models import Cart, FavoriteProduct
from menu.models import MenuItem, Logo

register = template.Library()


@register.inclusion_tag('header.html', takes_context=True)
def header(context):
    user = context.request.user
    items = MenuItem.objects.all()
    logo = Logo.objects.first()
    favorite_products_count = 0
    cart = None

    if user.id:
        cart = Cart.objects.get(user_id=user.id)
        favorite_products_count = len(FavoriteProduct.objects.filter(user_id=user.id))

    context = {
        'items': items,
        'cart': cart,
        'user': user,
        'logo': logo,
        'favorite_products_count': favorite_products_count,
    }

    return context


@register.inclusion_tag('footer.html')
def footer():
    info = Info.objects.first()
    logo = Logo.objects.first()
    menu_items = MenuItem.objects.all()
    social_items = SocialMedia.objects.all()

    context = {
        'info': info,
        'logo': logo,
        'menu_items': menu_items,
        'social_items': social_items,
    }

    return context
