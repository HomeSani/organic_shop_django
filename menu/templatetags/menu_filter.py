from django import template

from menu.models import MenuItem

register = template.Library()


@register.filter(name='get_children')
def get_children(value):
    """Return children of parent MenuItem"""

    return MenuItem.objects.filter(parent=value)
