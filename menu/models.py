from django.db import models

from menu.const import URLS


class MenuItem(models.Model):
    """Menu item(displayed links)"""

    name = models.CharField(max_length=32, unique=True, verbose_name='Название')
    url = models.CharField(max_length=32, choices=URLS, verbose_name='Путь')
    parent = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True,
                               verbose_name='Родительский объект меню')

    class Meta:
        verbose_name = "Элемент меню"
        verbose_name_plural = "Элементы меню"

    def __str__(self):
        return self.name


class Logo(models.Model):
    """Logo model, must be one"""
    image = models.ImageField(upload_to='menu/', verbose_name='Изображение')
    alt_text = models.CharField(max_length=16, verbose_name='Текст, если изображение не загрузится')
    url = models.CharField(max_length=32, choices=URLS, null=True, verbose_name='Путь при клике')

    class Meta:
        verbose_name = "Логотип"
        verbose_name_plural = "Логотип"

    def __str__(self):
        return self.alt_text
