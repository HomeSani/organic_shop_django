from django.contrib import admin

from menu.models import MenuItem, Logo

admin.site.register([MenuItem, Logo])
