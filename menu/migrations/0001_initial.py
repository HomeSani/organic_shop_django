# Generated by Django 4.2.1 on 2023-07-04 12:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Menu",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("company_phone", models.CharField(max_length=16)),
                ("logo", models.ImageField(upload_to="menu/")),
            ],
        ),
        migrations.CreateModel(
            name="MenuItem",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=32)),
                (
                    "url",
                    models.CharField(
                        choices=[
                            ("reset_confirm", "reset_confirm"),
                            ("send_reset_mail", "send_reset_mail"),
                            ("logout", "logout"),
                            ("account", "account"),
                            ("singin", "singin"),
                            ("singup", "singup"),
                            ("checkout", "checkout"),
                            ("cart", "cart"),
                            ("shop", "shop"),
                            ("product_detail", "product_detail"),
                            ("shop_index", "shop_index"),
                        ],
                        max_length=32,
                    ),
                ),
                (
                    "parent",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="menu.menuitem",
                    ),
                ),
            ],
        ),
    ]
