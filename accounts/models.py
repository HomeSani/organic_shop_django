from django.contrib.auth.models import User
from django.db import models


class Account(models.Model):
    """Model for accounts"""

    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name='Пользователь')
    first_name = models.CharField(max_length=128, verbose_name='Имя')
    last_name = models.CharField(max_length=128, verbose_name='Фамилия')
    image = models.ImageField(upload_to='account_images/', verbose_name='Иконка профиля')

    class Meta:
        verbose_name = "Аккаунт"
        verbose_name_plural = "Аккаунты"

    def __str__(self):
        return f'{self.first_name} {self.last_name} ({self.user})'
