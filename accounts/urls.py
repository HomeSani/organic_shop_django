from django.urls import path

from accounts.views import SingInView, SingUpView, AccountView, LogoutUser, ResetPasswordConfirm, ResetPasswordEmail

urlpatterns = [
    path('singup/', SingUpView.as_view(), name='singup'),
    path('singin/', SingInView.as_view(), name='singin'),
    path('account/', AccountView.as_view(), name='account'),
    path('logout/', LogoutUser.as_view(), name='logout'),
    path('send_reset_mail/', ResetPasswordEmail.as_view(), name='send_reset_mail'),
    path('reset/<str:uid>/<str:token>', ResetPasswordConfirm.as_view(), name='reset_confirm'),
]
