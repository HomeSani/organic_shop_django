from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.tokens import default_token_generator
from django.core.mail import send_mail
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views import View

from accounts.forms import SingUpForm, SingInForm, ResetPasswordForm, ResetPasswordEmailForm
from accounts.models import Account
from shop.models import Cart


def send_reset_password_email(user: User, email: str) -> None:
    """Send email with reset password link"""

    token = default_token_generator.make_token(user)
    uid = urlsafe_base64_encode(str(user.pk).encode('utf-8'))
    reset_link = f'http://127.0.0.1:8080/accounts/reset/{uid}/{token}'

    message = render_to_string('reset_password_email.html', {'reset_link': reset_link})

    if not settings.DEBUG:
        send_mail('Сброс пароля', message, 'noreplay@lol.com', [email])
    else:
        print(reset_link)


class SingUpView(View):
    def get(self, request):
        form = SingUpForm()

        return render(request, 'singup.html', {'form': form})

    def post(self, request):
        form = SingUpForm(request.POST, request.FILES)

        if form.is_valid():
            username = form.cleaned_data.get('username')
            email = form.cleaned_data.get('email')
            first_name = form.cleaned_data.get('first_name')
            last_name = form.cleaned_data.get('last_name')
            image = form.cleaned_data.get('image')
            password = form.cleaned_data.get('password1')

            user = User.objects.create_user(username=username, password=password, email=email)
            Account.objects.create(user=user, first_name=first_name, last_name=last_name, image=image)

            user = authenticate(username=username, password=password)
            login(request, user)

            return redirect(reverse('shop_index'))

        return redirect(reverse('singup'))


class SingInView(View):
    def get(self, request):
        return render(request, 'singin.html')

    def post(self, request):
        form = SingInForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')

            user = authenticate(username=username, password=password)
            login(request, user)

            Cart.objects.get_or_create(user_id=user.id)

            return redirect(reverse('shop_index'))

        return redirect(reverse('singin'))


class AccountView(View):
    def get(self, request):
        account = Account.objects.get(user=request.user)

        return render(request, 'account.html', {'account': account})


class LogoutUser(View):
    def get(self, request):
        logout(request)

        return redirect(reverse('singin'))


class ResetPasswordEmail(View):
    def get(self, request):
        return render(request, 'reset_password.html')

    def post(self, request):
        form = ResetPasswordEmailForm(request.POST)

        if form.is_valid():
            email = form.cleaned_data.get('email')
            user = User.objects.get(email=email)

            send_reset_password_email(user, email)

            return redirect(reverse('shop_index'))


class ResetPasswordConfirm(View):
    def get(self, request, uid, token):
        return render(request, 'new_password.html')

    def post(self, request, uid, token):
        form = ResetPasswordForm(request.POST)

        user_id = urlsafe_base64_decode(uid).decode('utf-8')
        user = User.objects.get(pk=user_id)

        if form.is_valid():
            password = form.clean_password1()

            if default_token_generator.check_token(user, token):
                user.set_password(password)
                user.save()

        return redirect(reverse('singin'))
