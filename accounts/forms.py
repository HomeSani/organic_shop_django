from django import forms


class SingUpForm(forms.Form):
    username = forms.CharField(max_length=128)
    email = forms.EmailField(max_length=256)
    first_name = forms.CharField(max_length=128)
    last_name = forms.CharField(max_length=128)
    image = forms.ImageField()
    password1 = forms.CharField(max_length=128)
    password2 = forms.CharField(max_length=128)

    def clean_password1(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('Пароли не совпадают')

        return password1


class ResetPasswordForm(forms.Form):
    password1 = forms.CharField(max_length=128)
    password2 = forms.CharField(max_length=128)

    def clean_password1(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('Пароли не совпадают')

        return password1


class ResetPasswordEmailForm(forms.Form):
    email = forms.EmailField(max_length=128)


class SingInForm(forms.Form):
    username = forms.CharField(max_length=64)
    password = forms.CharField(max_length=128)
