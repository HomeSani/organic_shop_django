## Ogania shop.
Pet project on Django and DRF.

### Configure project
1. Create **.env** file from **.env.example**.
2. Export variables from **.env**.
3. Configure media and static directories.
4. Install dependencies:
    ```shell
      poetry install
    ```
5. Make migrations:
   ```shell
      ./manage.py makemigrations
      ./manage.py migrate
   ```
6. Create superuser:
    ```shell
      ./manage.py createsuperuser
    ```
7. Create Account model for your superuser in admin panel.
8. Add some products and posts for testing.

### Main Features
1. Authorization and registration of users.
2. Password reset for authorized and non-authorized users.
3. Cart (adding, deleting products).
4. Creation of orders.
5. Fully translated admin panel.
6. Customizable header and footer.
7. Api for products and post(available in [http://your-domain/api/v1/]())
